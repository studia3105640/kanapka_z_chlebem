# KANAPKAzCHLEBEM_blog
Dzisiaj zajmiemy się przygotowaniem kanapki z niesamowitym nadzieniem, jakim jest ~~masło orzechowe~~ _**CHLEB**_.
## Lista potrzebnych rzeczy:
- Bułka
- Nóż
	- ostry
- **CHLEB**
- Talerz

**Ważny komunikat**
- Do kanapki z **CHLEBEM** pod żadnym pozorem nie dodajemy masła, ponieważ może ono zaburzyć konsystencję, tonację oraz pożądany w naszym daniu smak.

## Jak stworzyć taką kanapkę
1. Używamy noża, aby przekroić bułkę w poziomie
	1. _Uważamy, aby nie uszkodzić swojej skóry nożem, ponieważ może to być bolesne_
2. Pomiędzy dwie połowy bułki, starannie wkładamy kawałek **CHLEBA** _Jeżeli **CHLEB** nie został wcześniej pokrojony, to kroimy go nożem, lecz tym razem w pionie_
3. Zamykamy bułkę, kładąc jedną z połówek na **CHLEB**.


**Demonstracja składania takiej kanapki za pomocą kodu**
```
def knife(ingredient ,direction):
	if direction == horizontal:
		cut horizontally
	else:
		cut vertically
```
```
def kanapka_z_CHLEBEM(Amount):
	for i in range(Amount):
		cut(bułka, horizontally)
		cut(chleb, vertically)
		if bułka.amount == 2:
			put chleb between bułka[1] and bułka[2]
			return kanapka
		else:
			return NONE
```
Użycie
Takiej funkcji możemy używać na kuchennym blacie uruchamiając funkcję ``print(kanapka_z_chlebem(ilość kanapek))``, będzie smakować.

>smakuwa
![kanapka]()![KanapkazChlebem](https://gitlab.com/studia3105640/kanapka_z_chlebem/-/raw/main/kanapka%20z%20chlebem.jpg?ref_type=heads)
